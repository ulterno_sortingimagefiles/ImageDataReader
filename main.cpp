#include <QString>
#include <QStringList>
#include <QMultiMap>
#include <QDebug>
#include <iostream>
#include <string>

enum MIME {UNUSABLE = 0, PNG, JPG};

int main (int argc, char *argv[])
{
	QMultiMap<QString, QPair<int, QString>> imageInfo;
	while(true)
	{
		std::string iTemp;
		std::getline (std::cin, iTemp);
		QString currentEntry (QString::fromStdString(iTemp));
		// qDebug () << currentEntry;
		int number_of_spaces = 0;
		for (auto chr : currentEntry)
		{
			if (chr != ' ')
			{
				break;
			}
			number_of_spaces++;
		}
		imageInfo.insert(currentEntry.split(':').at(0).simplified(), qMakePair(number_of_spaces/2, currentEntry.simplified()));
		fflush(stdin);
		if (iTemp.empty())
		{
			break;
		}
	}

	// qDebug () << imageInfo;

	auto fileNameEntry = imageInfo.values ("Filename");
	QString fileName{};
	for (auto z : fileNameEntry)
	{
		if (z.first == 0)
		{
			fileName = z.second;
		}
	}
	auto infField = imageInfo.values ("Mime type");
	MIME usable_mime = UNUSABLE;
	for (auto x : infField)
	{
		if (x.second.contains("image/png"))
		{
			usable_mime = PNG;
			break;
		}
		else if (x.second.contains("image/jpeg"))
		{
			usable_mime = JPG;
			break;
		}
	}
	if (usable_mime == UNUSABLE)
	{
		return 1;
	}
	infField = imageInfo.values("Geometry");
	bool largeEnough = false;
	bool vertical;
	bool worked = false;
	for (auto g : infField)
	{
		if (g.first == 1)
		{
			worked = true;
			QStringList spll = g.second.split(':').at(1).simplified().split('+');
			QStringList res = spll.first().split('x');
			if (res.length() != 2)
			{
				qDebug () << "Resolution in key \"Geometry\" : " << res;
				worked = false;
				// return -1;
			}
			bool conv_to_intX, conv_to_intY;
			int X = res.at(0).toInt(&conv_to_intX);
			int Y = res.at(1).toInt(&conv_to_intY);
			if (!conv_to_intX || !conv_to_intY)
			{
				qDebug () << "Problem Converting Resolution to int";
				worked = false;
				// return -1;
			}
			qDebug () << X << Y << (X*Y);
			if (X > Y)
			{
				vertical = false; // Horizontal
				qDebug () << "Horizontal";
				switch (usable_mime)
				{
					case PNG:
						if (X > 1700 && ((X*Y) > 1000000))
						{
							largeEnough = true;
						}
						else
						{
							qDebug () << "Small";
						}
						break;
					case JPG:
						if (X > 1850 && ((X*Y) > 1300000))
						{
							largeEnough = true;
						}
						else
						{
							qDebug () << "Small";
						}
						break;
					default:
						break;
				}
			}
			else
			{
				vertical = true;
				qDebug () << "Vertical";
				switch (usable_mime)
				{
					case PNG:
						if (Y > 900 && (X*Y > 1000000))
						{
							largeEnough = true;
						}
						else
						{
							qDebug () << "Small";
						}
						break;
					case JPG:
						if (Y > 1000 && (X*Y > 1300000))
						{
							largeEnough = true;
						}
						else
						{
							qDebug () << "Small";
						}
						break;
					default:
						break;
				}
			}
			if (worked)
			{
				break;
			}
		}
	}
	if (!worked)
	{
		return -1;
	}
	if (!largeEnough)
	{
		std::cout << "small" << std::endl;
	}
	else if (vertical)
	{
		std::cout << "vertical" << std::endl;
	}
	else
	{
		std::cout << "horizontal" << std::endl;
	}

	return 0;
}
